import requests


def obtener_codigo_respuesta(url):
    try:
        response = requests.get(url)
        return response.status_code
    except requests.exceptions.RequestException as error:
        print("Error en la solicitud:", error)
        return None

# URL de Google
url_google = "https://www.google.com"

# Obtener y mostrar el código de respuesta
codigo_respuesta = obtener_codigo_respuesta(url_google)
if codigo_respuesta is not None:
    print("Código de respuesta de Google:", codigo_respuesta)
