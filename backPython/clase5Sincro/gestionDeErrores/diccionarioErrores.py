class SaldoNegativoException(Exception):
    def __init__(self,saldo):
        self.saldo= saldo
        super().__init__(f"El saldo no puede ser negativo : {saldo}")

class ClienteNotFoundException(Exception):
    def __init__(self,cliente):
        self.cliente= cliente
        super().__init__(f"El cliente no existe : {cliente}")