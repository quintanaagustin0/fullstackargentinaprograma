def dividirNumPositivos(a,b):
    #if a < 0 or b < 0:
    #    raise Exception("Estas dividiendo valores negativos")
    assert a>0 and b>0 , "Estas dividiendo valores negativos"
    print(a/b)

def proceso(a,b):
    dividirNumPositivos(a,b)
    print("Continua proceso..") 

# Error de tiempo de ejecución, división entre cero 
dividendo = -10
divisor = 0

try:
    proceso(dividendo,divisor)

except ZeroDivisionError as error:
    print("Error : ", error)
#except Exception as error:
except AssertionError as error:
    print("Error : ",error)
else:
    print("Salio todo okey")
finally:
    print("proceso finalizado")


# Error de lógica, proceso de transferencia

#class SaldoNegativoException(Exception):
#    def __init__(self,saldo):
#        self.saldo= saldo
#        super().__init__(f"El saldo no puede ser negativo : {saldo}")
import diccionarioErrores as err

def transferir(saldo):
    if saldo<0:
        #raise Exception(f"El saldo no puede ser negativo : {saldo}")
        raise err.SaldoNegativoException(saldo)
    
    return f"Saldo transferido : {saldo}"

try:
    print(transferir(-100))
#except Exception as err:
except err.SaldoNegativoException as err:
    print("Error: ",err)

print("Siguiente proceso")