class Vehiculo:
    def __init__(self, marca, modelo):
        self.__marca=marca
        self.__modelo=modelo
    
    def obtener_marca(self):
        return self.__marca
    
    def setear_marca(self, marca):
        self.__marca = marca
        return self.__marca

    def obtener_info(self):
        return f"Marca: {self.__marca} , Modelo: {self.__modelo}"

class Moto(Vehiculo):
    def __init__(self,marca, modelo, cilindrada):
        super().__init__(marca, modelo)
        self.__cilindrada=cilindrada
    
    def obtener_info(self):
        info_vehiculo= super().obtener_info()
        return f"{info_vehiculo}, Cilindrada: {self.__cilindrada}"
    
class Automovil(Vehiculo):
    def __init__(self,marca, modelo, color):
        super().__init__(marca, modelo)
        self.__color=color
    
    def obtener_info(self):
        info_vehiculo= super().obtener_info()
        return f"{info_vehiculo}, Color: {self.__color}"
    
class Concesionario:
    def __init__(self):
        self.__vehiculos = []

    def agregar_vehiculo(self, vehiculo):
        self.__vehiculos.append(vehiculo)

    def mostrar_inventario(self):
        print("Inventario del Concesionario:")
        for vehiculo in self.__vehiculos:
            print(vehiculo.obtener_info())
#-------

moto = Moto("Toyota","Etios","66918")
print(moto.obtener_info())

auto = Automovil("Toyota","Etios","rojo")
print(auto.obtener_info())

concesionario = Concesionario()
concesionario.agregar_vehiculo(moto)
concesionario.agregar_vehiculo(auto)
concesionario.mostrar_inventario()