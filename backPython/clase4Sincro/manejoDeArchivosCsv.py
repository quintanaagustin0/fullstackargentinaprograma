import csv

nombre_archivo_csv = "datos.csv"

try:
    with open(nombre_archivo_csv, "r", newline="") as archivo_csv:
        lector_csv = csv.reader(archivo_csv)
        for fila in lector_csv:
            print(fila)
except FileNotFoundError:
    print("No se encontró el archivo CSV.")
except:
    print("Ocurrió un error al leer el archivo CSV.")


#import csv

nombre_archivo_csv = "datos.csv"

try:
    with open(nombre_archivo_csv, "r", newline="") as archivo_csv:
        lector_csv = csv.DictReader(archivo_csv)
        for fila in lector_csv:
            print(fila)
except FileNotFoundError:
    print("No se encontró el archivo CSV.")
except:
    print("Ocurrió un error al leer el archivo CSV.")


#import csv

nombre_archivo_csv = "nuevo_archivo.csv"
datos = [["Nombre", "Edad"], ["Ana", 25], ["Juan", 30], ["María", 28]]

try:
    with open(nombre_archivo_csv, "w", newline="") as archivo_csv:
        escritor_csv = csv.writer(archivo_csv)
        escritor_csv.writerows(datos)
    print("Datos escritos en el archivo CSV.")
except:
    print("Ocurrió un error al escribir en el archivo CSV.")


#import csv

nombre_archivo_csv = "nuevo_archivo_dict.csv"
datos = [
    {"Nombre": "Ana", "Edad": 25},
    {"Nombre": "Juan", "Edad": 30},
    {"Nombre": "María", "Edad": 28}
]

try:
    with open(nombre_archivo_csv, "w", newline="") as archivo_csv:
        encabezados = ["Nombre", "Edad"]
        escritor_csv = csv.DictWriter(archivo_csv, fieldnames=encabezados)
        escritor_csv.writeheader()
        escritor_csv.writerows(datos)
    print("Datos escritos en el archivo CSV.")
except:
    print("Ocurrió un error al escribir en el archivo CSV.")


