nombre_archivo = "archivo_escritura.txt"

try:
    archivo = open(nombre_archivo, "w")
    archivo.write("Hola, esto es un ejemplo de escritura en archivo.\n")
    archivo.write("Podemos agregar múltiples líneas.\n")
    archivo.write("Cada vez que llamamos a write(), agrega contenido al archivo.")
except:
    print("Ocurrió un error al escribir en el archivo.")
finally:
    archivo.close()

nombre_archivo = "archivo_sobrescribir.txt"

try:
    archivo = open(nombre_archivo, "w")
    archivo.write("Este es el nuevo contenido que reemplazará al antiguo.\n")
    archivo.write("¡Cuidado! El contenido anterior se ha borrado.")
except:
    print("Ocurrió un error al escribir en el archivo.")
finally:
    archivo.close()

# Escritura en un archivo utilizando 'with'
nombre_archivo = "archivo_escritura_with.txt"

contenido_nuevo = "Este es el contenido nuevo."

with open(nombre_archivo, "w") as archivo:
    archivo.write(contenido_nuevo)
