nombre_archivo_origen = "imagen_original.jpg"
nombre_archivo_destino = "copia_imagen.jpg"

try:
    with open(nombre_archivo_origen, "rb") as archivo_origen:
        datos_imagen = archivo_origen.read()
        with open(nombre_archivo_destino, "wb") as archivo_destino:
            archivo_destino.write(datos_imagen)
            print("Imagen copiada exitosamente.")
except FileNotFoundError:
    print("No se encontró el archivo de origen.")
except:
    print("Ocurrió un error al copiar la imagen.")


class Registro:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

registros = [Registro("Ana", 25), Registro("Juan", 30), Registro("María", 28)]
nombre_archivo_binario = "registros.bin"

try:
    with open(nombre_archivo_binario, "wb") as archivo:
        for registro in registros:
            archivo.write(registro.nombre.encode("utf-8"))
            archivo.write(registro.edad.to_bytes(2, byteorder="big"))
    print("Registros escritos en el archivo binario.")
except:
    print("Ocurrió un error al escribir en el archivo binario.")
