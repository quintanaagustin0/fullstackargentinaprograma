import unittest

class Calculadora:
    def sumar(self, a, b):
        return a + b

    def restar(self, a, b):
        return a - b

class TestCalculadora(unittest.TestCase):
    def setUp(self):
        self.calculadora = Calculadora()

    def tearDown(self):
        self.calculadora = None

    def test_sumar(self):
        resultado = self.calculadora.sumar(3, 5)
        assert resultado == 8 #self.assertEqual(resultado, 8)

    def test_restar(self):
        resultado = self.calculadora.restar(10, 3)
        self.assertEqual(resultado, 7)

if __name__ == '__main__':
    unittest.main()
