# test_division.py
import calculator
import pytest
def test_assert_equal():
    resultado = calculator.dividir(8, 2)
    assert resultado == 4

def test_assert_true():
    resultado = calculator.dividir(10, 2)
    assert resultado > 0

def test_assert_greater():
    resultado = calculator.dividir(10, 3)
    assert resultado > 3

def test_assert_raises():
    #try:
    #    result=calculator.dividir(6, 0)
    #except ValueError as err:
    #    assert True
    with pytest.raises(ValueError):
        calculator.dividir(6, 0)
