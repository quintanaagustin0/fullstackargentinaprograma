#Funciones
def saludar(nombre):
    msg = "Hola mundo! "+nombre
    print(msg)

nombre='Salo'
saludar(nombre)

#Estructura de control
edad=19
if edad>=18:
    saludar('Ser mayor de 18')
else :
    saludar('Ser menor de 18')

numero=19
if numero%2==0:
    print("Es Par")
else :
    print("Es impar")

#bucles for , while

numeros = [1,2,3,4,5]
for num in numeros:
    print(num)

for pos in range(0,len(numeros)):
    print("pos :",pos)
    if numeros[pos]==3:
        print("numeros :",numeros[pos])

pos = 0
while numeros[pos]!=3:
    pos = pos + 1

print(numeros[pos])
