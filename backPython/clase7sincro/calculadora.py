def resta(a,b):
    return a - b

def suma(a,b):
    return a+b

def dividir(a,b):
    if b==0:
        raise ValueError("No se puede dividir por cero")
    return a/b
