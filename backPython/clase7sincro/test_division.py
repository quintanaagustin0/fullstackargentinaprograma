import calculadora
import pytest
def test_assert_equal():
    result = calculadora.dividir(8,2)
    assert result == 4

def test_assert_true():
    result = calculadora.dividir(8,2)
    assert result > 0

def test_assert_exception():
    with pytest.raises(ValueError):
        calculadora.dividir(6,0)
    