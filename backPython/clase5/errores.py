# Error de sintaxis, falta el paréntesis de cierre en print
#print("Hola, mundo"

# Error de tiempo de ejecución, división entre cero
dividendo = 10
divisor = 0
resultado = dividendo / divisor


# Error de lógica, calcular el promedio incorrectamente
notas = [85, 90, 78, 95, 88]
cantidad_notas = len(notas)
suma_notas = sum(notas)
promedio_incorrecto = cantidad_notas /suma_notas  # Esto no calcula el promedio correcto
