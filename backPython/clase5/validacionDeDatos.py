def area_rectangulo(base, altura):
    assert base > 0 and altura > 0, "Los lados deben ser números positivos"
    return base * altura

# Uso de la función con valores válidos
print(area_rectangulo(5, 8))

# Uso de la función con valores inválidos
#print(area_rectangulo(-5, 8))

try:
    print(area_rectangulo(-5, 8))
except AssertionError as err:
    print ("Error: ",err)


class Producto:
    def __init__(self, nombre, precio):
        self.nombre = nombre
        self.precio = precio

# Crear un producto con precio válido
producto1 = Producto("Lápiz", 0.5)
assert producto1.precio > 0, "El precio debe ser positivo"

# Crear un producto con precio inválido
producto2 = Producto("Libro", -10)
assert producto2.precio > 0, "El precio debe ser positivo"
