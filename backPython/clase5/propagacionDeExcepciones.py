# Propagación de excepciones hacia arriba 
def funcion_b():
    # Esta función divide entre cero, generando una excepción
    resultado = 10 / 0

def funcion_a():
    funcion_b()
    print("Se dividio correctamente")

try:
    funcion_a()
except ZeroDivisionError as error:
    print("Error: División entre cero.")


class SaldoNegativoException(Exception):
    def __init__(self,saldo):
        self.saldo = saldo
        super().__init__(f"El Saldo no puede ser negativo : {saldo}")


# Uso de la cláusula raise para generar una excepción
def transferir(saldo):
    if saldo < 0:
        raise SaldoNegativoException(saldo)
        #raise Exception("El Saldo no puede ser negativo : "+saldo)
    return f"Saldo transferido: {saldo}"


try:
    print(transferir(-100))
#except Exception as error:
#    print("Error:", error)
except SaldoNegativoException as error:
    print("Error:",error)
