# Creación de conjuntos
conjunto1 = {1, 2, 3, 4, 5}
conjunto2 = {4, 5, 6, 7, 8}

# Unión de conjuntos
union = conjunto1.union(conjunto2)
print("Unión:", union)  # Salida: {1, 2, 3, 4, 5, 6, 7, 8}

# Intersección de conjuntos
interseccion = conjunto1.intersection(conjunto2)
print("Intersección:", interseccion)  # Salida: {4, 5}

# Diferencia entre conjuntos
diferencia1 = conjunto1.difference(conjunto2)
diferencia2 = conjunto2.difference(conjunto1)
print("Diferencia conjunto1 - conjunto2:", diferencia1)  # Salida: {1, 2, 3}
print("Diferencia conjunto2 - conjunto1:", diferencia2)  # Salida: {6, 7, 8}

# Los conjuntos son útiles para eliminar elementos duplicados de una lista.
# Por ejemplo, si tienes una lista con elementos duplicados:

lista_con_duplicados = [1, 2, 2, 3, 4, 4, 5, 5, 5]

# Convierte la lista en un conjunto para eliminar duplicados
conjunto_sin_duplicados = set(lista_con_duplicados)

# Convertir el conjunto nuevamente en una lista (opcional, si necesitas una lista sin duplicados)
lista_sin_duplicados = list(conjunto_sin_duplicados)

print("Lista original:", lista_con_duplicados)  # Salida: [1, 2, 2, 3, 4, 4, 5, 5, 5]
print("Lista sin duplicados:", lista_sin_duplicados)  # Salida: [1, 2, 3, 4, 5]
