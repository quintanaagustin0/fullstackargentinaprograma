# Creación de una tupla
mi_tupla = (1, 2, 3, 4, 5)

# Acceso a elementos de la tupla (por índice)
primer_elemento = mi_tupla[0]  # Obtendrá el valor 1
segundo_elemento = mi_tupla[1]  # Obtendrá el valor 2
tercer_elemento = mi_tupla[2]  # Obtendrá el valor 3

# Mostrar la tupla completa
print(mi_tupla)  # Salida: (1, 2, 3, 4, 5)

# Puedes usar tuplas para representar datos constantes que no deben modificarse.
# Por ejemplo, días de la semana:
dias_semana = ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo")

# Acceso a elementos de la tupla
print(dias_semana[0])  # Salida: Lunes
print(dias_semana[6])  # Salida: Domingo

# Intento de modificación (esto generaría un error)
#dias_semana[0] = "Monday"  # Generaría un TypeError, ya que las tuplas son inmutables

# Las tuplas son útiles para funciones que deben devolver múltiples valores.
# Por ejemplo, una función que calcula el área y el perímetro de un rectángulo:

def calcular_area_perimetro(lado_a, lado_b):
    area = lado_a * lado_b
    perimetro = 2 * (lado_a + lado_b)
    return (area, perimetro)  # Devolvemos una tupla con ambos valores

# Llamamos a la función y recibimos los valores en una tupla
resultado = calcular_area_perimetro(5, 8)
print(resultado)  # Salida: (40, 26)

# Acceso a los valores individuales
area_resultado = resultado[0]
perimetro_resultado = resultado[1]
print("Área:", area_resultado)  # Salida: Área: 40
print("Perímetro:", perimetro_resultado)  # Salida: Perímetro: 26
