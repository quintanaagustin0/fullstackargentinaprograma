# Creación de una lista vacía
mi_lista = []

# Agregar elementos a la lista
mi_lista.append(1)
mi_lista.append(2)
mi_lista.append(3)
mi_lista.append(4)
mi_lista.append(5)

# Acceso a elementos de la lista (por índice)
primer_elemento = mi_lista[0]  # Obtendrá el valor 1
segundo_elemento = mi_lista[1]  # Obtendrá el valor 2
tercer_elemento = mi_lista[2]  # Obtendrá el valor 3

# Modificación de elementos en la lista
mi_lista[1] = 5  # El segundo elemento ahora será 5

# Eliminación de elementos en la lista
del mi_lista[0]  # Eliminamos el primer elemento
mi_lista.remove(tercer_elemento)
elemento_eliminado = mi_lista.pop(len(mi_lista)-1)
mi_lista.clear()

# Mostrar la lista actual
print(mi_lista) 

# Longitud de la lista
longitud = len(mi_lista)
print("Longitud de la lista:", longitud) 
