#El polimorfismo es la capacidad de un objeto para tomar diferentes formas o comportarse de diferentes maneras.


import math

class Figura:
    def calcular_area(self):
        pass

class Circulo(Figura):
    def __init__(self, radio):
        self.radio = radio

    def calcular_area(self):
        return math.pi * self.radio ** 2

class Rectangulo(Figura):
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura

    def calcular_area(self):
        return self.base * self.altura

circulo = Circulo(5)
rectangulo = Rectangulo(4, 6)

figuras = [circulo, rectangulo]

for figura in figuras:
    print(f"Área de la figura: {figura.calcular_area()}")


class Animal:
    def hacer_sonido(self):
        pass

class Perro(Animal):
    def hacer_sonido(self):
        return "Woof, woof!"

class Gato(Animal):
    def hacer_sonido(self):
        return "Meow, meow!"

animales = [Perro(), Gato()]

for animal in animales:
    print(animal.hacer_sonido())
