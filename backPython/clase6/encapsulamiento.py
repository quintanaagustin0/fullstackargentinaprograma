# El encapsulamiento es un principio de la programación orientada a objetos que se refiere a ocultar la implementación interna de una clase y exponer solo la interfaz necesaria.


class Persona:
    def __init__(self, nombre, edad):
        self.__nombre = nombre  # Atributo privado
        self.__edad = edad  # Atributo privado

    def obtener_nombre(self):
        return self.__nombre

    def establecer_edad(self, nueva_edad):
        if nueva_edad > 0:
            self.__edad = nueva_edad

    def mostrar_informacion(self):
        print(f"Nombre: {self.__nombre}, Edad: {self.__edad}")

# Crear una instancia de Persona
persona = Persona("Juan", 30)

# Intentar acceder a los atributos privados directamente
# Esto causará un error, ya que los atributos son privados
# print(persona.__nombre)
# print(persona.__edad)

# Acceder a los atributos privados utilizando métodos públicos
print("Nombre:", persona.obtener_nombre())
persona.establecer_edad(35)
persona.mostrar_informacion()
