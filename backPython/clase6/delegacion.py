# La delegación es un concepto donde un objeto transfiere la ejecución de ciertas tareas a otro objeto asociado.

class Motor:
    def encender(self):
        print("Motor encendido")

    def apagar(self):
        print("Motor apagado")

class Coche:
    def __init__(self):
        self.motor = Motor()  # Instancia de la clase Motor

    def encender(self):
        print("Iniciando el proceso de encendido del coche")
        self.motor.encender()

    def apagar(self):
        print("Iniciando el proceso de apagado del coche")
        self.motor.apagar()

# Crear una instancia de Coche
mi_coche = Coche()

# Encender y apagar el coche utilizando la delegación
mi_coche.encender()
mi_coche.apagar()
