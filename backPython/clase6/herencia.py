#Permite crear nuevas clases basadas en clases existentes, heredando sus atributos y métodos

class Vehiculo:
    def __init__(self, marca, modelo):
        self.marca = marca
        self.modelo = modelo

    def obtener_info(self):
        return f"Marca: {self.marca}, Modelo: {self.modelo}"

class Automovil(Vehiculo):
    def __init__(self, marca, modelo, color):
        # Llamamos al constructor de la superclase utilizando super()
        super().__init__(marca, modelo)
        self.color = color

    def obtener_info(self):
        info_padre = super().obtener_info()  # Llamamos al método de la superclase
        return f"{info_padre}, Color: {self.color}"

auto = Automovil("Toyota", "Corolla", "Rojo")
print(auto.obtener_info())


import math

class Figura:
    def calcular_area(self):
        pass

class Circulo(Figura):
    def __init__(self, radio):
        self.radio = radio

    def calcular_area(self):
        return math.pi * self.radio ** 2

class Cuadrado(Figura):
    def __init__(self, lado):
        self.lado = lado

    def calcular_area(self):
        return self.lado ** 2

circulo = Circulo(5)
cuadrado = Cuadrado(4)

print(f"Área del círculo: {circulo.calcular_area()}")
print(f"Área del cuadrado: {cuadrado.calcular_area()}")
