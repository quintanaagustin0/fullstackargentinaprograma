# Introduccion a Django

## Crear un Entorno Virtual y Instalar Django

1. Crear un directorio para tu proyecto:

Abre tu terminal y navega al directorio donde deseas crear tu proyecto y tu entorno virtual.

2. Crear el Entorno Virtual:

Ejecuta el siguiente comando para crear un entorno virtual con venv (reemplaza nombre_del_entorno con el nombre que desees para el entorno):

    python -m venv .venv

observacion, si el comando "python" no se encuentra, intentar con "py".

3. Activar el Entorno Virtual:

Activa el entorno virtual en la terminal:

* En Windows:

      .venv\Scripts\activate

* En macOS y Linux:

      source .venv/bin/activate

observacion, posible error:
    .venv\Scripts\activate : No se puede cargar el archivo C:\Users\PC\Desktop\clase salo djamgop\.venv\Scripts\Activate.ps1 porque la ejecución de scripts está deshabilitada en este sistema.
solucion:
    Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy RemoteSigned

Verás el nombre de tu entorno virtual en el prompt de la terminal, lo que indica que estás dentro del entorno virtual.


4. Actualizar pip (opcional pero recomendado):

Antes de instalar Django, es una buena práctica asegurarse de que pip esté actualizado:

    python -m pip install --upgrade pip

5. Instalar Django:

Una vez que el entorno virtual esté activado y pip esté actualizado, puedes instalar Django:

    pip install django


6. Crear un Proyecto Django:

Dirígete al directorio donde deseas crear el proyecto y ejecuta:

    django-admin startproject sitio_prueba

La carpeta sitio_prueba contiene a su vez
otra carpeta sitio_prueba junto a un archivo llamado manage.py. Este archivo
es un script que sirve para gestionar todo nuestro proyecto desde la terminal.
Por otro lado el subdirectorio sitio_prueba es el que contiene los scripts base del
proyecto, los cuales tienen toda la configuración inicial y de despliegue.

* __init.py__ nos indica que la carpeta es un paquete.

* settings.py contiene todos los ajustes del sitio. Es donde registramos
todas las aplicaciones que creamos, la localización de nuestros archivos
estáticos, los detalles de configuración de la base de datos, etc.

* urls.py define los mapeos url-vistas. A pesar de que éste podría contener
todo el código del mapeo url, es más común delegar algo del mapeo a
las propias aplicaciones.

* wsgi.py se usa para ayudar a la aplicación Django a comunicarse con el
servidor web. Puedes tratarlo como código base que puedes utilizar de
plantilla.

Ejecutamos manage.py en terminal para poner en marcha el servidor.

    cd mi_proyecto
    python manage.py runserver

7. Base de datos

La configuración [settings.py](./sitio_prueba/sitio_prueba/settings.py) por defecto para utilizar SQLite3 es simplemente el ENGINE y
una ruta a la carpeta donde se creará la base de datos, que por defecto es el
directorio principal del proyecto y se llamará db.sqlite3

Y para sincronizarla con el proyecto, ejecutamos:

    python manage.py migrate


8. Aplicaciones en django :

Django propone un sistema de reutilización de código organizado en apps, son
aplicaciones internas que implementan funcionalidades específicas.

Las Apps activas en un proyecto de Django, las encontramos definidas en el
archivo de configuración [settings.py](./sitio_prueba/sitio_prueba/settings.py), en la lista INSTALLED_APPS 

Django permite no solo incluir apps genéricas sino que también nos permite
crear las nuestras.

Ve al directorio del proyecto y crea una aplicación:

    python manage.py startapp prueba

Dentro de nuestra aplicación prueba, vamos a encontrar varios archivos

[views.py](./sitio_prueba/prueba/views.py)
Este archivo es uno de los más importantes y en él se definen las vistas de la app. Una vista hace referencia a la lógica que se ejecuta cuando se hace una petición a nuestra web. 

[Urls.py](./sitio_prueba/sitio_prueba/urls.py)
Dentro del archivo urls.py establecemos un path indicando la URL donde vamos
a enlazar una vista de la app prueba que a su vez estará devolviendo una
respuesta HTML.

[Templates](./sitio_prueba/prueba/templates/prueba/home.html)
Django nos ofrece la posibilidad de utilizar plantillas HTML (en inglés templates)
más fáciles de usar y con más funcionalidades.

Luego de crear el template , se actualiza [settings.py](./sitio_prueba/sitio_prueba/settings.py) añadiendo la app en la lista INSTALLED_APPS.

Y finalmente actualizamos la vista [views.py](./sitio_prueba/prueba/views.py)


Podemos decir que una app es una aplicación web que implementa
una funcionalidad y un proyecto es un conjunto de configuraciones a las que se
"conectan" esas apps para que todo unido de lugar a un sitio web completo.



8. Desactivar el Entorno Virtual:

Cuando hayas terminado de trabajar con Django, puedes desactivar el entorno virtual en cualquier momento ejecutando:

    deactivate


Siguiendo estos pasos, podrás crear y utilizar un entorno virtual utilizando venv y comenzar a trabajar en proyectos Django de manera aislada y organizada.

## Mi primer app MVC con Django

Definiendo un Modelo


